# Portfolio 2.0

## 🚀 Build

- Node 14.15.0 and NPM
- Gatsby 4.3.0
- Google analytics 4.3.0
- React 17.0.1

1.  **Gatsby Portfolio**

    Used the Gatsby CLI to create a new portfolio

    ```shell
    npm init gatsby
    ```

2.  **Execute**

    ```shell
    cd portfolio/
    gatsby develop || npm run develop
    ```

3.  **Open**

    Your site is now running at http://localhost:8000!

4.  **More**

    - [Documentation](https://www.gatsbyjs.com/docs/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

## 🚀 Deployed on [Gatsby Cloud](https://www.gatsbyjs.com/cloud/)
